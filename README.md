Namenskonvention:

YYYYMMDDID-DB-User.sql.

Beispiel: Fix von "Leupi" an Datenbank world am 5.3.2013:

"2013030500-world-leupi-01.sql"

Die ID dient dazu, mehrere Updates voneinander zu unterscheiden und 
beginnt bei 00! 
