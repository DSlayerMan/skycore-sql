REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (30061, 40753, 100, 1, 0, 2, 2);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (30061, 2, 100, 1, 3, -90903, 2);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (30061, 1, 100, 1, 0, -34136, 3);

DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=40753;
DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=2;
DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=1;
DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=45912;

REPLACE INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `faction`, `flags`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `AIName`, `ScriptName`, `WDBVerified`) VALUES (909004, 3, 1387, 'Kel\'Thuzad Truhe', '', '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 1634, 30061, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 1);
REPLACE INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `faction`, `flags`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `AIName`, `ScriptName`, `WDBVerified`) VALUES (909003, 3, 1387, 'Kel\'Thuzad Truhe', '', '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 1634, 15990, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 1);

REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (15990, 40618, 0, 1, 1, 1, 1);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (15990, 40617, 0, 1, 1, 1, 1);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (15990, 40616, 0, 1, 1, 1, 1);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (15990, 40753, 100, 1, 0, 1, 2);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (15990, 1, 100, 1, 0, -34044, 2);

DELETE FROM `creature_loot_template` WHERE  `entry`=15990 AND `item`=40618;
DELETE FROM `creature_loot_template` WHERE  `entry`=15990 AND `item`=40617;
DELETE FROM `creature_loot_template` WHERE  `entry`=15990 AND `item`=40616;
DELETE FROM `creature_loot_template` WHERE  `entry`=15990 AND `item`=40753;
DELETE FROM `creature_loot_template` WHERE  `entry`=15990 AND `item`=1;
DELETE FROM `creature_loot_template` WHERE  `entry`=15990 AND `item`=45912;