UPDATE `creature_template` SET `ScriptName`='npc_tenebron' WHERE  `entry`=30452 LIMIT 1;
UPDATE `creature_template` SET `ScriptName`='npc_shadron' WHERE  `entry`=30451 LIMIT 1;
UPDATE `creature_template` SET `ScriptName`='npc_vesperon' WHERE  `entry`=30449 LIMIT 1;

UPDATE `creature_template` SET `unit_flags`=33685510 WHERE  `entry`=30648 LIMIT 1;

UPDATE `creature_template` SET `mechanic_immune_mask`=650854235 WHERE  `entry`=28860 LIMIT 1;

UPDATE `creature_template` SET `mechanic_immune_mask`=650854235 WHERE  `entry`=31311 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_acolyte_of_shadron' WHERE  `entry`=31218 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_acolyte_of_vesperon' WHERE  `entry`=31219 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_twilight_whelp' WHERE  `entry`=30890 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_twilight_whelp' WHERE  `entry`=31214 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_twilight_eggs' WHERE  `entry`=30882 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31218 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31219 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=30882 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31214 LIMIT 1;

UPDATE `creature_template` SET `mechanic_immune_mask`=617291772 WHERE  `entry`=30453 LIMIT 1;

UPDATE `creature_template` SET `mechanic_immune_mask`=33554432 WHERE  `entry`=30681 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_twilight_eggs ' WHERE  `entry`=31204 LIMIT 1;

UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `faction_A`=103, `faction_H`=103 WHERE  `entry`=31204 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31539 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31547 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31541 LIMIT 1;

UPDATE `creature_template` SET `faction_H`=103, `faction_A` = 103 WHERE  `entry`=31543 LIMIT 1;

UPDATE `creature_template` SET `mindmg`=464, `maxdmg`=604, `attackpower`=708, `dmg_multiplier`=5 WHERE  `entry`=30890 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_Onyx_Blaze_Mistress' WHERE  `entry`=30681 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_Onyx_Brood_General' WHERE  `entry`=30680 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_Onyx_Flight_Captain' WHERE  `entry`=30682 LIMIT 1;

UPDATE `creature_template` SET `ScriptName`='npc_Onyx_Sanctum_Guardian' WHERE  `entry`=30453 LIMIT 1;

UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80 WHERE  `entry`=31214 LIMIT 1;

UPDATE `creature_template` SET `faction_A`=103, `faction_H`=103 WHERE  `entry`=31548 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=27 WHERE  `entry`=30453 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=10 WHERE  `entry`=30681 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=16 WHERE  `entry`=30682 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=19 WHERE  `entry`=30680 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=16 WHERE  `entry`=31218 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=15 WHERE  `entry`=31219 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=3 WHERE  `entry`=30643 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=26 WHERE  `entry`=30999 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=7 WHERE  `entry`=31540 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=25 WHERE  `entry`=31541 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=29 WHERE  `entry`=31001 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=19 WHERE  `entry`=31000 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=25 WHERE  `entry`=31543 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=3 WHERE  `entry`=31317 LIMIT 1;

UPDATE `creature_template` SET `dmg_multiplier`=7 WHERE  `entry`=31548 LIMIT 1;

UPDATE `creature_template` SET `mindmg`=464, `maxdmg`=604, `attackpower`=708, `dmg_multiplier`=7 WHERE  `entry`=31214 LIMIT 1;