INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES (0, 57391, 64, 'Disable LOS for 4 horsemen punish spell');

INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES (0, 57377, 64, 'Disable LOS for 4 horsemen punish spell');

UPDATE `creature_template` SET `mechanic_immune_mask`=617299803 WHERE  `entry`=30549;
UPDATE `creature_template` SET `mechanic_immune_mask`=617299803 WHERE  `entry`=30600;